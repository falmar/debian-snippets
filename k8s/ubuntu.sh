# master
apt-get update && apt-get install -y apt-transport-https curl \
&& curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - \
&& echo -e "deb http://apt.kubernetes.io/ kubernetes-xenial main\n" > /etc/apt/sources.list.d/kubernetes.list \
&& apt-get update \
&& apt-get install -y kubelet kubeadm kubectl \
&& apt-mark hold kubelet kubeadm kubectl

# node
apt-get update && apt-get install -y apt-transport-https curl \
&& curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - \
&& echo -e "deb http://apt.kubernetes.io/ kubernetes-xenial main\n" > /etc/apt/sources.list.d/kubernetes.list \
&& apt-get update \
&& apt-get install -y kubelet kubeadm \
&& apt-mark hold kubelet kubeadm
