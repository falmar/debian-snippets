FROM debian:9.3-slim
RUN apt-get update \
    && apt-get install -y curl \
    && curl -sSL https://rexray.io/install | sh -s -- stable 0.11.2 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

CMD ["rexray", "start", "--service", "dobs"]
